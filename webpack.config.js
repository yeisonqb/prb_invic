const { VueLoaderPlugin } = require("vue-loader");
const miniCssExtractPlugin = require("mini-css-extract-plugin")
const HtmlWebpackPlugin = require("html-webpack-plugin")
const Dotenv = require("dotenv-webpack");

module.exports = {
    mode: process.env.NODE_ENV || "development",
    entry: ['babel-polyfill','./src/index.js'],
    output: {
        path: __dirname + '/dist',
        filename: 'js/bundle.js'
    },

    module:{
        rules:[
            {
                test: /\.js$/,
                exclude: /node_modules/,    
                use:{
                    loader: 'babel-loader'
                }
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            },
            {
                test: /\.(css|sass|scss)$/,
                use: [miniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
            },
            {
                test: /\.(ico|png|jpg|jpeg|gif|svg|webp)$/,
                loader: 'file-loader',
                options:{
                    name: 'media/img/[name].[ext]'
                }
            },
            {
                test: /\.(mp4|ogg)$/,
                loader: 'file-loader',
                options:{
                    name: 'media/video/[name].[ext]'
                }
            },
            {
                test: /\.(woff|woff2|ttf|eot)$/,
                loader: "file-loader",
                options: {
                    name: "media/fonts/[name].[ext]",
                },
            }
        ]
    },
    resolve: {
        extensions: [ '.tsx', '.ts', '.js', '.vue' ],
        alias: {
            'vue': '@vue/runtime-dom'
        }
    },
    plugins:[
        new VueLoaderPlugin(),
        new HtmlWebpackPlugin({
            template: "./src/index.html"
        }),
        new miniCssExtractPlugin({
            filename: "styles/styles.css",
        }),
        new Dotenv(),
    ],
    devServer: {
        https: true,
        port: 3400,
        historyApiFallback: true,
        host: "localhost.org",
    },
    devtool: "source-map",
};