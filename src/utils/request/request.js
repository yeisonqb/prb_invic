import axios from 'axios'

const ReqMovies = async({category, type, page})=>{
    const res = await axios.get(`https://api.themoviedb.org/3/${category}/${type}`, {
            params: {
                'api_key': '1c3e2e9bce43d529ff3474586c70b2d9',
                'page': page
            }
        }).then((res)=>{
            return res
        }).catch((err)=>{
            console.error(err)
        })
    
    return res.data
}

export default ReqMovies