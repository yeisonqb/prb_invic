import Vue, { createApp } from 'vue'
import App from './App.vue'

import { library } from '@fortawesome/fontawesome-svg-core'
import { faCircle, faPlayCircle } from '@fortawesome/free-regular-svg-icons'
import {faTwitter, faFacebookSquare, faGooglePlusSquare} from '@fortawesome/free-brands-svg-icons'
import { faHeart, faCog, faUserCircle, faPlus, faSearch, faStar, 
        faAlignJustify, faTh, faChevronCircleUp, faChevronCircleDown, 
        faTimes, faThumbsDown, faTrophy} from '@fortawesome/free-solid-svg-icons'

library.add(
    faTh, faCog, faPlus, faStar, faTimes, faHeart,
    faCircle, faSearch, faTrophy, faTwitter, faThumbsDown,
    faPlayCircle, faUserCircle, faAlignJustify, faFacebookSquare,
    faChevronCircleUp, faGooglePlusSquare, faChevronCircleDown
);

import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'




//export app
createApp(App).component('font-awesome-icon', FontAwesomeIcon).mount('#app')

